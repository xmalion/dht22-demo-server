#include <SimpleDHT.h>
#include <SerialCommand.h>
// for DHT22, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: D2

#define DEBUG_LED LED_BUILTIN   // Arduino LED on board
#define SERIALCOMMAND_HARDWAREONLY 1 // 
byte pinDHT22 = 2;
SimpleDHT22 dht22;
SerialCommand SCmd;

void setup()
{
  Serial.begin(9600);
  Serial.println("# Server with DHT22 Sensor Demo #");  

  pinMode(DEBUG_LED, OUTPUT);      // Configure the onboard LED for output
  digitalWrite(DEBUG_LED, HIGH);   // default to LED off
  initSerialCommands(); // see init_commands.ino     
}

#define MAX_ITERATIONS 1000
#define ITERATION_SPEED_IN_msec 10
int iteration = MAX_ITERATIONS;

void loop()
{   
/*
  if (iteration >= MAX_ITERATIONS)
  {
    getTemperature();
    iteration = 0;
  }
  delay(ITERATION_SPEED_IN_msec);
  iteration++;  */
}

void serialEvent(){
   SCmd.readSerial(); 
}

