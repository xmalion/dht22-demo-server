void initSerialCommands() {
  // Setup callbacks for SerialCommand commands 
  SCmd.addCommand("HELLO",SayHello);           // Echos the string argument back
  
  SCmd.addCommand("ON", LED_on);                // Turns LED on
  SCmd.addCommand("on", LED_on);          // Turns LED on
  
  SCmd.addCommand("OFF", LED_off);        // Turns LED off  
  SCmd.addCommand("off", LED_off);        // Turns LED off
  
  SCmd.addCommand("t", getTemperature);        // get temperature and humidity
  SCmd.addCommand("temperature", getTemperature);        // get temperature and humidity
  
  SCmd.addDefaultHandler(unrecognized);  // Handler for command that isn't matched  (says "What?") 
}
