// This gets set as the default handler, and gets called when no other command matches. 
void unrecognized()
{
  Serial.println("What?"); 
}

void SayHello()
{
  char *arg;  
  arg = SCmd.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL)      // As long as it existed, take it
  {
    Serial.print("Hello "); 
    Serial.println(arg); 
  } 
  else {
    Serial.println("Hello, whoever you are"); 
  }
}

void LED_on()
{
  Serial.println("LED on"); 
  digitalWrite(DEBUG_LED, HIGH);  
}

void LED_off()
{
  Serial.println("LED off"); 
  digitalWrite(DEBUG_LED, LOW);
}
